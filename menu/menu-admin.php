<!doctype html>
 <html class="no-js" lang="" ng-app="app"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>SIAISA</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/animate.css">
        <link rel="stylesheet" href="../css/menu-admin.css?id=1.0">
        <link rel="stylesheet" href="../css/rome.css">
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body ng-controller="menuCtrl">
        <?php
                require_once '../php/model/MenuModel.php';
                $menu = new MenuModel();
         ?>
 
        <div id="menu" class="panel" role="navigation">
            <span class="blanco"> 
                <img src="../img/logo-siaisa.png" alt="logo-siaisa"  width="35" height="35"/>
                        <?php echo strtoupper($menu->imprimirNombre());   ?>
             </span>
            <ul>
               <?php if(!$menu->isFinanciera()):?>  
                <li><a href="../encuesta/"><span class="fa fa-list-ul fa-2x nav-icon"></span>ENCUESTA</a></li>
                <li><a href="contacto.html"><span  class="fa fa-calendar-check-o fa-2x nav-icon"></span>COBRANZA </a></li>
                <li><a href="../busqueda/"><span class="fa fa-search fa-2x nav-icon"></span>BÚSQUEDA</a></li>
                <li><a href="../alta/"><span class="fa fa-user-plus fa-2x nav-icon"></span>ALTA AGENTE</a></li>
                <li><a href="../monitoreo/"><span class="fa fa-line-chart fa-2x nav-icon"></span>MONITOREO</a></li>
                <li><a href="../reporte.php?tipo=final"><span class="fa  fa-arrow-down fa-2x nav-icon"></span>REPORTE FINAL</a></li>
               <?php endif?>
                <li><a href="../emailing/"><span class="fa  fa fa-upload fa-2x nav-icon"></span>CARGAR LAYOUT EMAIL</a></li>
                <li><a href="../logout.php"><span class="fa fa-sign-out fa-2x nav-icon"></span>CERRAR SESIÓN</a></li>
            </ul>

        </div>
        
         <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
              <div class="navbar-header">
                 <!--TODO modificar estilo en responsive-->
                  <a href="#menu" style="color: white; margin-top: 10px" class="menu-link pull-left navbar-brand">
                      <span>
                          <i class="fa fa-bars fa-1x" aria-hidden="true"></i>
                          <?php 
                           echo strtoupper($menu->imprimirNombre());   
                           
                           ?>
                      </span>    
                          
                              
                  </a>
             
              </div>
          
   </nav>
      
        <div class="centro-loader" ng-hide="angularLoaded">
            <img src="../img/rolling.svg" alt="loader" />
        </div>
        <div class="wrap push container-fluid" ng-cloak ng-show="angularLoaded" style="padding-top: 80px">
           
                
           <div class="row">
               
               
                  <div class="col-md-6 col-sm-12" style="<?php echo "display:". ($menu->isAdmin()==true? "block":"none")?>">
                      <div>
                          <div class="col-md-12 col-sm-12">
                              <div class="col-md-8 col-sm-8 col-xs-8">
                                  <label>Contactación hoy</label>
                                  <br class="visible-xs">
                                  <select>
                                      <option>Encuesta</option>
                                      <option>Cobranza</option>
                                  </select>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                  <button type="button" ng-click="onUpdate()" class="btn btn-default btn-circle" style="margin-bottom:20px">
                                         <i class="glyphicon glyphicon-refresh"></i>
                                   </button>  
                              </div>
                            </div>
                          <div class="col-md-12 no-padding">
                            <canvas id="bar"  class="chart chart-bar animacion"
                                    chart-data="data" chart-labels="labels"  chart-series="series">
                             </canvas> 
                          </div>
                            <div class="col-md-12 no-padding">
                              <canvas id="bar" class="chart chart-bar"
                                chart-data="data_motivo" chart-labels="labels_motivo" chart-options="options_motivo" >
                              </canvas> 
                          </div>
                          
                          <div class="col-md-12 no-padding">
                              <canvas id="base" class="chart-horizontal-bar"
                                chart-data="data_cal" chart-labels="labels_cal" chart-options="options" >
                              </canvas> 
                          </div>
                          
                             
                        
                      </div>
                    
             </div>
           
               <div class="col-md-6 col-sm-12">
                    <div class="col-md-4 no-padding hidden-xs">
                         <input id='inputInicio' placeholder="Fecha inicio" class='input form-control input-sm'  />       
                     </div>
                     <div class="col-md-4 no-padding hidden-xs">
                          <input id='inputFin' placeholder="Fecha fin" class='input form-control input-sm'  />      
                     </div>
                     <div class="col-md-4 no-padding hidden-xs">
                         <a class="btn btn-danger btn-block btn-sm" ng-click="onFiltrar()">{{filtrarText}}</a>   
                     </div>
                   <div class="col-md-12">
                     <table  id="tablaReporte" class=" table table-responsive table-striped table-bordered">
                      <thead>
                          <tr>
                              <th>N°</th>
                              <th>Agente</th>
                              <th>Llamadas</th>
                              <th>Contactos</th>
                              <th>Citas</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="agente in list_monitoreo">
                            <td>{{$index+1}}</td>
                            <td>{{agente.nombre|uppercase}}</td>
                            <td>{{agente.total_contactados}}</td>
                            <td>{{agente.contactados}}</td>   
                            <td>{{agente.citas}}</td>
                         </tr>
                         <tr>
                            <td></td>
                            <td><strong>TOTAL:</strong></td>
                            <td><strong>{{total_llamadas}}</strong></td>
                            <td><strong>{{total_contactados}}</strong> </td> 
                            <td><strong>{{total_citas}}</strong></td>
                        </tr>
                      </tbody>
                  </table>  
                   </div>
                                       

                   <div class="col-md-12 col-sm-12">
                          <ng-include  src="'../html/menu_info.html'"></ng-include>
                    </div>
                   
                    <div class="col-md-12 col-sm-12 no-padding">
                    <canvas id="bar" class="chart chart-bar"
                                chart-data="data_financiera" chart-labels="labels_financiera" chart-options="options_financiera" >
                    </canvas> 
               </div>
               </div>
            
                
             
          
                  
           </div>
         

        </div>

       

        <script src="../js/vendor/wow.min.js"></script>
       <!-- Angular Material Library -->
       <script src="..//js/vendor/jquery-1.11.2.js"></script>
       <script src="../js/vendor/bootstrap.min.js"></script>
       <script src="../js/vendor/bigSlide.min.js"></script>
       <script>
        $(document).ready(function() {
            $('.menu-link').bigSlide({
                easyClose:true
            });
            $("#menu").css("visibility","visible");
        });
    </script>
      <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular.min.js"></script>
       <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-animate.min.js"></script>
       <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-aria.min.js"></script>
       <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-messages.min.js"></script>
       <script src="https://jtblin.github.io/angular-chart.js/node_modules/chart.js/dist/Chart.min.js"></script>
       <script src="http://cdn.jsdelivr.net/angular.chartjs/latest/angular-chart.min.js"></script>
       <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-1.3.3.js"></script>
       <script src="../js/angular/app.js?id=1.5.11"></script>
       <script src="../js/vendor/rome.js"></script>
       <script src="../js/angular/controladores/menuCtrl.js?id=1.5.11"></script>
       
    </body>
</html>