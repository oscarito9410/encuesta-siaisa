
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>COBRANZA</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/main_menu.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/animate.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body>
    <?php 
    require_once '../php/model/MenuModel.php'; 
    require_once '../php/model/EmailModel.php';
    $menu=new MenuModel(); 
    $email= new EmailModel();
    $subs=$email->getSubscriptores();
    
    ?>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand animacion" href="#"><span><img src="../img/logo-siaisa.png" width="35" height="35"/></span>
                   <?php 
                           echo $menu->imprimirNombre();
                   ?>
                  </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="../logout.php">CERRAR SESIÓN</a>
                    </li>
                </ul>

            </div>
    </nav>
    <div class="container-fluid" style="padding-top:100px">
        <div class="row">
            <div class="col-md-5">
                 <div class="col-md-12">
                     <h4>Nota* El archivo debe ser .csv delimitado por comas</h4>
                 </div>
                   <div class="col-md-12">
                        <form id="subir">
                            <input type="file" id="csv" name="csv" accept=".csv" class="filestyle" data-buttonText="Elegir archivo"/>
                            <br>
                            <div class="col-md-6">
                                <img style="visibility:hidden" id="progress" src="../img/ellipsis.svg"/>
                            </div>
                            <input type="submit" id="#upload"    class="btn btn-danger" value="Enviar archivo"/>
                            <input type="button" id="enviarMail" class="btn btn-danger" value="Enviar email"/>
                            
                        </form>

                    </div>
                   <div class="col-md-12">
                        <div id="respuesta" style="display: none">
                            <h3>Enviado correctamente</h3>
                            <h4>
                                Total de registros <strong><i id="total" style="color: red"></i></strong>

                            </h4>
                        </div>

                     </div>
            </div>
            
            <div class="col-md-7">
                <?php if (is_array($subs) || is_object($subs)):?>
                <?php if(!empty($subs)):?>
                Total de subscriptores <strong><i id="total" style="color: red"><?php echo count($subs)?></i></strong>
                      <table id="tablaReporte" class=" table table-responsive table-striped table-bordered">
                      <thead>
                          <tr>
                              <th>N°</th>
                              <th>Nombre</th>
                              <th>Email</th>
                          </tr>
                      </thead>
                      <tbody>
                           <?php foreach ($subs as $s):?>
                          <tr>
                          
                            <td><?php echo $s['no_contrato']?></td>
                            <td><?php echo $s['nombre']?></td>
                            <td><?php echo $s['email']?></td>
                         </tr>
                          <?php endforeach?>
                      </tbody>
                  </table>
                  <?php endif?>
                  <?php endif?> 
            </div>
       
    
    
        </div>
    </div>
    <footer>
        <span>2016 SIAISA</span>
    </footer>

    <script src="../js/vendor/wow.min.js"></script>
    <!-- Angular Material Library -->
    <script src="../js/vendor/jquery-1.11.2.js"></script>
    <script src="../js/vendor/bootstrap.min.js"></script>
    <script src="../js/vendor/bootstrap-filestyle.min.js"></script>
    <script>
    $("#enviarMail").click(function(){
            $('#progress').css('visibility','visible');
             $.ajax({
                        url: '../php/api_bench.php', 
                        contentType: false,
                        processData: false,                       
                        type: 'post',
                        success: function(response){
                            $("#progress").fadeOut();
                            $("#respuesta").css("display","block");
                            alert(response);
                        },
                       error:function(response){
                            alert(response.responseText);
                            console.log(response);
                            $("#progress").fadeOut();
                       }
             });
    })
    $('#subir').submit(function(e) {
            e.preventDefault();
            $('#progress').css('visibility','visible');

            var file_data = $('#csv').prop('files')[0];   
            var form_data = new FormData();                  
            form_data.append('csv', file_data);                           
            $.ajax({
                        url: '../php/api/emailing/enviar_csv.php', 
                        dataType: 'text', 
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,                         
                        type: 'post',
                        success: function(response){
                            $("#progress").fadeOut();
                            $("#respuesta").css("display","block");
                            $("#total").text(response);
                        },
                       error:function(response){
                            alert(response.responseText);
                            console.log(response);
                            $("#progress").fadeOut();
                       }
             });
        });
    </script>
</body>

</html>